 Head | Information |
| ------ | ----------- |
| Logbook entry number | 01 |
| Name  | Kee Soon Win |
| Matrix no.  | 195901 |
| Year  | 4 |
| Subsystem  | Simulation |
| Group  | 7 |
| Date  | 25/10/2021-29/10/2021 |


## Tables

| Target | Information |
| ------ | ----------- |
| Agenda  | -The very first meeting of the group members of the simulation subsystem group<br> -After discussion among group members we comes out with group Progress Gantt Chart<br> -We also discussed the timeline of the progress in simulations (Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys
| Decision | -We dicide to jot down everything progress in gantt chart<br> -Due to pandamic we only sent few members to the lab<br> -Team up with members to learn Gitlab |
| Method | -Team members volunteer themselves to go to the lab<br> -We have made the schedule for the members to split what to do|
| Impact| -Limitation of the students going to the lab due to lab SOP<br> -Gitlab was new for us to work with |
| Next step | -Go to the lab to take the airship measurement |

